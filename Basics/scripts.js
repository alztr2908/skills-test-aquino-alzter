// GLOBALS
const USERNAME = "serge"
const PWD = "123"

const username = document.querySelector("#username")
const password = document.querySelector("#password")
const icon = document.querySelector("#icon")
const submit = document.querySelector("#submit-btn")

// event for making eye visible when typing password feature
password.addEventListener("focus", function (e) {
    if (e.value != "") {
        icon.style.display = "block"
    }
})

password.addEventListener("input", function () {
    if (this.value != "") {
        icon.style.display = "block"
    } else {
        icon.style.display = "none"
    }
})

// event for making eye icon toggleable
icon.addEventListener("click", function () {
    this.style.display = "block"
    // toggle the type attribute
    const type =
        password.getAttribute("type") === "password" ? "text" : "password"
    password.setAttribute("type", type)

    // toggle the icon (toggling ain't working)
    if (this.classList.contains("bi-eye")) {
        this.classList.remove("bi-eye")
        this.classList.add("bi-eye-slash")
    } else if (this.classList.contains("bi-eye-slash")) {
        this.classList.remove("bi-eye-slash")
        this.classList.add("bi-eye")
    }
})

// event for form validation
submit.addEventListener("click", function () {
    const validator = document.querySelector(".validation")
    const message = document.querySelector(".validation-message")

    // Remove previous input
    validator.classList.remove("is-success")
    validator.classList.remove("is-failed")

    if (username.value == USERNAME && password.value == PWD) {
        validator.style.visibility = "visible"
        validator.classList.add("is-success")
        message.textContent = "Successfully logged in."
    } else {
        validator.style.visibility = "visible"
        validator.classList.add("is-failed")
        message.textContent = "Invalid login details"
    }
})

// prevent form submit
const form = document.querySelector("form")
form.addEventListener("submit", function (e) {
    e.preventDefault()
})
