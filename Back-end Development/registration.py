class Person:
    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name
    
    def sleep(self):
        return f"{self.first_name} {self.last_name} is now sleeping"

class Student(Person):
    def __init__(self, first_name, last_name, student_number):
        super().__init__(first_name, last_name)
        self.student_number = student_number
        self.is_registered = False
    
    def sleep(self):
        return f"Student {self.student_number} is now sleeping"
        

class Administrator(Person):
    def __init__(self, first_name, last_name):
        super().__init__(first_name, last_name)
    
    def sleep(self):
        return f"{self.first_name} {self.last_name} is now sleeping"
    
    def register(self,obj):
        if obj.is_registered == False:
            obj.is_registered = True
            return f"Student {obj.student_number} is registered"
        elif obj.is_registered == True:
            return f"Student {obj.student_number} is already registered"

def main():
    student = Student("First Name", "Last Name", "1234567")
    admin = Administrator("UP", "CRS")
    print(student.sleep())
    print(admin.sleep())
    print(admin.register(student))
    print(admin.register(student))    

if __name__ == "__main__":
    main()
